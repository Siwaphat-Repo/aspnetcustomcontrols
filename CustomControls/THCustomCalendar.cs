﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calendar = System.Web.UI.WebControls.Calendar;
namespace CustomControls
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:THCustomCalendar runat=server></{0}:THCustomCalendar>")]
    public class THCustomCalendar : CompositeControl
    {
        //protected ListBox DateListBox = new ListBox();
        //protected ListBox MonthListBox = new ListBox();
        //protected ListBox YearListBox = new ListBox();
        //private UpdatePanel updatePanal = new UpdatePanel();
        protected TextBox DateTextBox = new TextBox();
        protected Calendar calendar = new Calendar();
        protected Button calendarButton = new Button();
        protected Button OKButton = new Button();
        protected Button CancelButton = new Button();

        public THCustomCalendar()
        {
            ////initial date.
            //int dateCounter = 1;
            //while (dateCounter < 31)
            //{
            //    DateListBox.Items.Add(dateCounter.ToString()); 
            //    dateCounter++;
            //}

            //List<string> monthNames = new List<string>();
            //monthNames.Add("มกราคม");
            //monthNames.Add("กุมภาพันธ์");
            //monthNames.Add("มีนาคม");
            //monthNames.Add("เมษายน");
            //monthNames.Add("พฤษภาคม");
            //monthNames.Add("มิถุนายน");
            //monthNames.Add("กรกฎาคม");
            //monthNames.Add("สิงหาคม");
            //monthNames.Add("กันยายน");
            //monthNames.Add("ตุลาคม");
            //monthNames.Add("พฤศจิกายน");
            //monthNames.Add("ธันวาคม");

            ////initial mounth names.
            //foreach(string monthName in monthNames)
            //{
            //    MonthListBox.Items.Add(monthName);
            //}

            ////initial years
            //int startyear = 2490;
            //while(startyear < (DateTime.Now.AddYears(20).Year + 543))
            //{
            //    YearListBox.Items.Add(startyear.ToString());
            //    startyear++;
            //}


            OKButton.Text = "ยืนยัน";
            CancelButton.Text = "ยกเลิก";
        }



        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }
            set
            {
                ViewState["Text"] = value;
            }
        }


        //[Category("Appearance")]
        //[Description("Gets or sets the selected date of custom calendar control")]
        //public DateTime SelectedDate
        //{
        //    get
        //    {
        //        EnsureChildControls();
        //        return string.IsNullOrEmpty(DateTextBox.Text) ? DateTime.MinValue : Convert.ToDateTime(DateTextBox.Text);
        //    }

        //    set
        //    {
        //        if (value != null)
        //        {
        //            EnsureChildControls();
        //            DateTextBox.Text = value.ToShortDateString();
        //            calendar.SelectedDate = value.Date;
        //            calendar.VisibleDate = value.Date;
        //        }
        //        else
        //        {
        //            EnsureChildControls();
        //            DateTextBox.Text = "";
        //        }
        //    }
        //}

        private DateTime _SelectedDate;
        [Category("Appearance")]
        [Bindable(true)]
        [Localizable(true)]
        [Description("Gets or sets the selected date of custom calendar control")]
        public DateTime SelectedDate
        {
            get
            {
                DateTime dt = (DateTime)ViewState["SelectedDate"];
                return dt;
            }
            set
            {
                _SelectedDate = value;
                if (value != null)
                {
                    //EnsureChildControls();
                    calendar.SelectedDate = value.Date;
                    calendar.VisibleDate = value.Date;
                    ViewState["SelectedDate"] = value;
                    DateTextBox.Text = value.ToString("dd MMMM yyyy", new CultureInfo("th-TH"));
                }
                else
                {
                    //EnsureChildControls();
                    ViewState["SelectedDate"] = value;
                }
            }
        }


        //protected override void RenderContents(HtmlTextWriter writer)
        //{
        //    writer.Write(Text);
        //}

        //protected override void RecreateChildControls()
        //{
        //    EnsureChildControls();
        //}

        protected override void CreateChildControls()
        {
            //DateListBox.ID = "DateListBox";
            //MonthListBox.ID = "MonthListBox";
            //YearListBox.ID = "YearListBox";

            Controls.Clear();

            DateTextBox.Enabled = false;

            calendarButton.Width = Unit.Pixel(50);
            calendarButton.Text = "เลือก";

            calendarButton.Click += CalendarButton_Click;

            calendar.Visible = false;
            calendar.SelectedDayStyle.ForeColor = System.Drawing.Color.Blue;
            calendar.SelectedDayStyle.BackColor = System.Drawing.Color.Gray;
            calendar.SelectedDayStyle.BorderWidth = Unit.Pixel(1);
            calendar.SelectionChanged += Calendar_SelectionChanged;

            OKButton.Visible = false;


            CancelButton.Visible = false;

            this.Controls.Add(DateTextBox);
            this.Controls.Add(calendarButton);
            this.Controls.Add(calendar);
            this.Controls.Add(OKButton);
            this.Controls.Add(CancelButton);

            //base.CreateChildControls();
        }



        private void CalendarButton_Click(object sender, EventArgs e)
        {
            calendar.Visible = !calendar.Visible;
            OKButton.Visible = !OKButton.Visible;
            CancelButton.Visible = !CancelButton.Visible;
        }

        private void Calendar_SelectionChanged(object sender, EventArgs e)
        {
            if (calendar.SelectedDate != null)
            {
                DateTime getDate = calendar.SelectedDate;
                //DateTextBox.Text = calendar.SelectedDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                DateTextBox.Text = getDate.ToString("dd MMMM yyyy", new CultureInfo("th-TH"));
                SelectedDate = getDate;
                calendar.Visible = false;
            }
        }



        protected override void Render(HtmlTextWriter writer)
        {
            AddAttributesToRender(writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "1");

            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            DateTextBox.RenderControl(writer);
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            calendarButton.RenderControl(writer);
            writer.RenderEndTag();

            writer.RenderEndTag();

            writer.RenderEndTag();

            calendar.RenderControl(writer);

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            OKButton.RenderControl(writer);
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            CancelButton.RenderControl(writer);
            writer.RenderEndTag();

        }
    }
}
