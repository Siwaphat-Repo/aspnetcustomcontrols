﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestControl
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                THCustomCalendar1.SelectedDate = DateTime.Now;
            }
        }

        protected void Btn_PostBack_Click(object sender, EventArgs e)
        {
            var getSelectedDate = THCustomCalendar1.SelectedDate;
        }
    }
}